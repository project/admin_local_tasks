<?php

namespace Drupal\admin_local_tasks\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure admin_local_tasks specific settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_local_tasks_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'admin_local_tasks.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('admin_local_tasks.settings');

    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Appearance'),
      '#open' => TRUE,
    ];

    $form['appearance']['position'] = [
      '#type' => 'select',
      '#options' => [
        'left' => $this->t('Left'),
        'right' => $this->t('Right'),
      ],
      '#title' => $this->t('Position'),
      '#default_value' => $config->get('position') ? $config->get('position') : 'left',
      '#description' => $this->t('Select screen side where links will be shown.'),
    ];

    $form['appearance']['tooltips'] = [
      '#type' => 'select',
      '#options' => [
        $this->t('Disabled'),
        $this->t('Enabled'),
      ],
      '#title' => $this->t('Tooltips'),
      '#default_value' => $config->get('tooltips'),
      '#description' => $this->t('If enabled, title of link will be shown as tooltip instead.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('admin_local_tasks.settings');

    $config->set('position', $form_state->getValue('position'));
    $config->set('tooltips', $form_state->getValue('tooltips'));
    $config->save();

    Cache::invalidateTags(['admin_local_tasks:settings']);

    parent::submitForm($form, $form_state);
  }

}
