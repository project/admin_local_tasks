<?php

/**
 * @file
 * Documentation for Admin Local Tasks module APIs.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Modify the list of route links class mapping.
 *
 * This hook may be used to modify link class for custom icons and styling.
 *
 * return @param $mapping
 *   An array of mappings.
 */
function hook_local_tasks_mapping_alter(array &$mapping) {
  $mapping['version_history'] = 'revisions';
}

/**
 * @} End of "addtogroup hooks".
 */
