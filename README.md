# Admin Local Tasks

Helper module to make Drupal (Admin) Local Task links fancier.

- For a full description of the module, visit the [project page](https://www.drupal.org/project/admin_local_tasks).
- Use the [Issue queue](https://www.drupal.org/project/issues/admin_local_tasks) to submit bug reports and feature suggestions, or track changes.


## Contents of this file

 - Requirements
 - Installation
 - Configuration
 - Maintainers


## Requirements

 * Latest dev release of Drupal 8.x.


## Installation

 * Install as you would normally install a contributed Drupal module. See [installing modules](https://www.drupal.org/node/895232) for further information.


## Configuration

Users in roles with the 'Administer Admin Local Tasks' permission will be able
to manage the module settings. Configure permissions
as usual at:

* Administration » People » Permissions
* admin/people/permissions

From the module settings page, configure the block appearance. See:

* Administration » Configuration » User Interface » Admin Local Tasks
* admin/config/user-interface/admin-local-tasks


## Maintainers

Current maintainers:
 * Matej Lehotsky - [@matej.lehotsky](https://www.drupal.org/u/matejlehotsky)
