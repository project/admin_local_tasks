(function ($, Drupal) {
  Drupal.behaviors.adminLocalTasksTooltips = {
    attach: function attach(context) {
      var selector = '.block-local-tasks-block.tooltips';
      var options = {placement: 'right', offset: [0, 16]};

      if ($(selector).hasClass('position-right')) {
        options.placement = 'left';
      }

      tippy(selector + ' a[data-tippy-content]', options);
    }
  }
})(jQuery, Drupal);
